/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public class DateAndTimeUtil {

  public static Date getLocalDateTimeAsDate(LocalDateTime localDateTime) {
    return (localDateTime == null)
        ? null
        : Date.from((localDateTime.atZone(ZoneId.systemDefault())).toInstant());
  }

  public static LocalDateTime getDateAsLocalDateTime(Date date) {
    return (date == null)
        ? null
        : LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  }

  public static LocalDateTime getLocalDateTimeFromEpochTimeInMilliseconds(
      long epochTimeInMilliSeconds) {
    return Instant.ofEpochMilli(epochTimeInMilliSeconds)
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime();
  }
}
