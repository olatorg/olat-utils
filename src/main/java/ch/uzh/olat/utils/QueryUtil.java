/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.utils;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public class QueryUtil {

  /**
   * Default size to be used when partitioning big lists into smaller chunks to avoid PostgreSQL
   * problems, see {@see #collectionOfChunks(List list)}. This number must be <= 16'384.
   */
  private static final int DEFAULT_CHUNK_SIZE = 15000;

  /**
   * Big lists must be partitioned because PostgreSQL can not handle so many elements in IN
   * operator, see https://makk.es/blog/postgresql-parameter-limitation/.
   *
   * <p>Note: the limit 30'000 as maximum number of query parameters proposed in the blog is too
   * large, the correct value for the limit is 2^14 = 16'384 (see
   * QueryParametersBindingImpl#expandListValuedParameters and PGStream#sendInteger2).
   *
   * <p>Solution taken from
   * https://e.printstacktrace.blog/divide-a-list-to-lists-of-n-size-in-Java-8/.
   */
  public static <T> Collection<List<T>> collectionOfChunks(List<T> list) {
    return collectionOfChunks(list, DEFAULT_CHUNK_SIZE);
  }

  /**
   * Use this method in case the query has multiple IN operators or in case the DB keys in the IN
   * operator are composed (e.g. keys of a relation table). In such cases a smaller value than
   * DEFAULT_CHUNK_SIZE has to be used.
   */
  public static <T> Collection<List<T>> collectionOfChunks(List<T> list, int chunkSize) {
    AtomicInteger counter = new AtomicInteger();
    return list.stream()
        .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / chunkSize))
        .values();
  }
}
